using System.Linq;
using System.Net;
using API.ClientModels.Errors;
using ClientModels.Errors;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace API.Services
{
    public static class ErrorResponsesService
    {
        public static string GetErrorMessage(ModelStateDictionary modelState)
        {
            var error = modelState.Keys.SelectMany(key => modelState[key].Errors).FirstOrDefault();
            return error != null ? error.ErrorMessage : "Ошибка. Ошибок не обнаружено ._.";
        }

        public static ServiceErrorResponse BadRequest(string target, string message)
        {
            return new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code =  ServiceErrorCodes.BadRequest,
                    Message = message,
                    Target = target
                }
            };
        }
        
        public static ServiceErrorResponse BodyIsMissing(string target)
        {
            return new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code =  ServiceErrorCodes.BadRequest,
                    Message = "Request body or some required fields are empty.",
                    Target = target
                }
            };
        }

        public static ServiceErrorResponse DuplicationError(string target, string message)
        {
            return new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code =  ServiceErrorCodes.BadRequest,
                    Message = message,
                    Target = target
                }
            };
        }

        public static ServiceErrorResponse NotFoundError(string target, string message)
        {
            return new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.NotFound,
                Error = new ServiceError
                {
                    Code =  ServiceErrorCodes.NotFound,
                    Message = message,
                    Target = target
                }
            };
        }
        
        public static ServiceErrorResponse InvalidDateTimeFormat(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Date format must be 'yyyy-MM-dd' and time format is 'hh:mm:ss'.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse InvalidImageData(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.BadRequest,
                    Message = "Image must has PNG or JPG format and be less than 512 KB.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse InvalidCredentialsError(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.InvalidCredentials,
                    Message = "Username or password is incorrect.",
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse ValidationError(string target, string message)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.ValidationError,
                    Message = message,
                    Target = target
                }
            };

            return error;
        }
        
        public static ServiceErrorResponse UnauthorizedError(string target)
        {
            var error = new ServiceErrorResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Error = new ServiceError
                {
                    Code = ServiceErrorCodes.Unauthorized,
                    Message = "You must be authorized. Access prohibited.",
                    Target = target
                }
            };

            return error;
        }
    }
}