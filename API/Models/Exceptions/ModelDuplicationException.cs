﻿using System;

namespace API.Models.Exceptions
{
    public class ModelDuplicationException : Exception
    {
        public ModelDuplicationException(string modelName, Guid id)
            : base($"{modelName} with id '{id}' already exists.")
        {
        }
        
        public ModelDuplicationException(string modelName, string name)
            : base($"{modelName} with name '{name}' already exists.")
        {
        }
    }
}