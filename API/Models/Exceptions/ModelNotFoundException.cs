﻿using System;

namespace API.Models.Exceptions
{
    public class ModelNotFoundException : Exception
    {
        public ModelNotFoundException(string modelName, Guid id)
            : base($"{modelName} with id '{id}' not found.")
        {
        }
        
        public ModelNotFoundException(string modelName, string name)
            : base($"{modelName} with name '{name}' not found.")
        {
        }
    }
}