﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Departments;
using API.ClientModels.Positions;

namespace API.Models.Positions.Repositories
{
    public interface IPositionsRepository
    {
        Task<Position> CreateAsync(PositionCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<Position> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<Position> GetFromDepartmentByName(IReadOnlyList<Guid> departmentPositionIds, string name,
            CancellationToken cancellationToken);
        Task<Position[]> GetAllFromDepartment(IReadOnlyList<Guid> departmentPositionIds, CancellationToken cancellationToken);
        Task<Position[]> GetAllWithIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken);
    }
}