﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Positions;
using API.Models.Exceptions;
using MongoDB.Driver;

namespace API.Models.Positions.Repositories
{
    public class MongoPositionsRepository : IPositionsRepository
    {
        private readonly IMongoCollection<Position> positions;

        public MongoPositionsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("AviahackDb");
            positions = database.GetCollection<Position>("Position");
        }

        public Task<Position> CreateAsync(PositionCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var position = positions.Find(item => item.Name.Equals(creationInfo.Name) && 
                                                  item.Department.Equals(creationInfo.Department)).FirstOrDefault();

            if (position != null)
            {
                throw new ModelDuplicationException(nameof(Position), creationInfo.Name);
            }

            var newId = Guid.NewGuid();
            var now = DateTime.UtcNow;
            var newPosition = new Position
            {
                Id = newId,
                Name = creationInfo.Name,
                Department = creationInfo.Department,
                CreatedAt = now,
                UpdatedAt = now
            };

            positions.InsertOne(newPosition, cancellationToken: cancellationToken);
            return Task.FromResult(newPosition);
        }

        public Task<Position> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var position = positions.Find(item => item.Id == id).FirstOrDefault();

            if (position == null)
            {
                throw new ModelNotFoundException(nameof(Position), id);
            }

            return Task.FromResult(position);
        }

        public Task<Position> GetFromDepartmentByName(IReadOnlyList<Guid> departmentPositionIds, string name,
            CancellationToken cancellationToken)
        {
            if (departmentPositionIds == null)
            {
                throw new ArgumentNullException(nameof(departmentPositionIds));
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            
            foreach (var positionId in departmentPositionIds)
            {
                var position = positions.Find(item => item.Id == positionId && item.Name.Equals(name))
                    .FirstOrDefault();

                if (position != null)
                {
                    return Task.FromResult(position);
                }
            }

            throw new ModelNotFoundException(nameof(Position), name);
        }

        public Task<Position[]> GetAllFromDepartment(IReadOnlyList<Guid> departmentPositionIds, CancellationToken cancellationToken)
        {
            if (departmentPositionIds == null)
            {
                throw new ArgumentNullException(nameof(departmentPositionIds));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var set = new HashSet<Guid>(departmentPositionIds);
            var departmentPositions = positions.Find(item => set.Contains(item.Id)).ToEnumerable().ToArray();
            return Task.FromResult(departmentPositions);
        }

        public Task<Position[]> GetAllWithIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken)
        {
            if (ids == null)
            {
                throw new ArgumentNullException(nameof(ids));
            }
            
            var set = new HashSet<Guid>(ids);
            var result = positions
                .Find(item => set.Contains(item.Id))
                .ToEnumerable()
                .ToArray();
            return Task.FromResult(result);
        }
    }
}