﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Departments;

namespace API.Models.Departments.Repositories
{
    public interface IDepartmentsRepository
    {
        Task<Department> CreateAsync(DepartmentCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<Department> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<Department> GetByNameAsync(string name, CancellationToken cancellationToken);
        Task<Department[]> GetAllAsync(CancellationToken cancellationToken);
        Task<Department[]> GetAllWithIdsAsync(IReadOnlyList<Guid> ids, CancellationToken cancellationToken);
        Task AddPositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken);
        Task DeletePositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken);
        Task AddProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken);
        Task DeleteProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken);
    }
}