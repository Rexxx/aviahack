﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Departments;
using API.Models.Exceptions;
using MongoDB.Driver;

namespace API.Models.Departments.Repositories
{
    public class MongoDepartmentsRepository : IDepartmentsRepository
    {
        private readonly IMongoCollection<Department> departments;

        public MongoDepartmentsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("AviahackDb");
            departments = database.GetCollection<Department>("Department");
        }

        public Task<Department> CreateAsync(DepartmentCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var department = departments.Find(item => item.Name.Equals(creationInfo.Name)).FirstOrDefault();

            if (department != null)
            {
                throw new ModelDuplicationException(nameof(Department), creationInfo.Name);
            }

            var newId = Guid.NewGuid();
            var now = DateTime.UtcNow;
            var newDepartment = new Department
            {
                Id = newId,
                Name = creationInfo.Name,
                Positions = new Guid[0],
                Projects = new Guid[0], 
                CreatedAt = now,
                UpdatedAt = now
            };

            departments.InsertOne(newDepartment, cancellationToken: cancellationToken);
            return Task.FromResult(newDepartment);
        }

        public Task<Department> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var department = departments.Find(item => item.Id == id).FirstOrDefault();

            if (department == null)
            {
                throw new ModelNotFoundException(nameof(Department), id);
            }

            return Task.FromResult(department);
        }

        public Task<Department> GetByNameAsync(string name, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            var department = departments.Find(item => item.Name.Equals(name)).FirstOrDefault();

            if (department == null)
            {
                throw new ModelNotFoundException(nameof(Department), name);
            }

            return Task.FromResult(department);
        }

        public Task<Department[]> GetAllAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var result = departments.Find(item => true).ToEnumerable().ToArray();
            return Task.FromResult(result);
        }

        public Task<Department[]> GetAllWithIdsAsync(IReadOnlyList<Guid> ids, CancellationToken cancellationToken)
        {
            if (ids == null)
            {
                throw new ArgumentNullException(nameof(ids));
            }
            
            var set = new HashSet<Guid>(ids);
            var result = departments
                .Find(item => set.Contains(item.Id))
                .ToEnumerable()
                .ToArray();
            return Task.FromResult(result);
        }

        public async Task AddPositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Department>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Department>.Update.Push("Positions", positionId).Set("UpdatedAt", DateTime.UtcNow);
            await departments.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        public async Task DeletePositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Department>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Department>.Update.Pull("Positions", positionId).Set("UpdatedAt", DateTime.UtcNow);
            await departments.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        public async Task AddProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Department>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Department>.Update.Push("Projects", projectId).Set("UpdatedAt", DateTime.UtcNow);
            await departments.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }

        public async Task DeleteProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Department>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Department>.Update.Pull("Projects", projectId).Set("UpdatedAt", DateTime.UtcNow);
            await departments.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        }
    }
}