using System;
using System.Collections.Generic;
using AspNetCore.Identity.Mongo.Model;

namespace API.Models.Users
{
    public class User : MongoUser
    {
        public string Name { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public IReadOnlyList<string> Projects { get; set; }
        public IReadOnlyList<Guid> CompletedTutorials { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime LastUpdateAt { get; set; }
    }
}