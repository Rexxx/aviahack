namespace API.Models.Users
{
    public class UserPatchInfo
    {
        public string Username { get; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public UserPatchInfo(string username, string password = null, string confirmPassword = null)
        {
            Username = username;
            Password = password;
            ConfirmPassword = confirmPassword;
        }
    }
}