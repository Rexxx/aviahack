﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Projects;
using API.Models.Exceptions;
using MongoDB.Driver;

namespace API.Models.Projects.Repositories
{
    public class MongoProjectsRepository : IProjectsRepository
    {
        private readonly IMongoCollection<Project> projects;

        public MongoProjectsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("AviahackDb");
            projects = database.GetCollection<Project>("Project");
        }

        public Task<Project> CreateAsync(ProjectCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var project = projects.Find(item => item.Name.Equals(creationInfo.Name) && 
                                                item.Department.Equals(creationInfo.Department)).FirstOrDefault();

            if (project != null)
            {
                throw new ModelDuplicationException(nameof(Project), creationInfo.Name);
            }

            var newId = Guid.NewGuid();
            var now = DateTime.UtcNow;
            var newProject = new Project
            {
                Id = newId,
                Name = creationInfo.Name,
                Department = creationInfo.Department,
                CreatedAt = now,
                UpdatedAt = now
            };

            projects.InsertOne(newProject, cancellationToken: cancellationToken);
            return Task.FromResult(newProject);
        }

        public Task<Project> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var project = projects.Find(item => item.Id == id).FirstOrDefault();

            if (project == null)
            {
                throw new ModelNotFoundException(nameof(Project), id);
            }

            return Task.FromResult(project);
        }

        public Task<Project> GetFromDepartmentByName(IReadOnlyList<Guid> departmentProjectIds, string name,
            CancellationToken cancellationToken)
        {
            if (departmentProjectIds == null)
            {
                throw new ArgumentNullException(nameof(departmentProjectIds));
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            
            cancellationToken.ThrowIfCancellationRequested();
            
            foreach (var projectId in departmentProjectIds)
            {
                var project = projects.Find(item => item.Id == projectId && item.Name.Equals(name))
                    .FirstOrDefault();

                if (project != null)
                {
                    return Task.FromResult(project);
                }
            }

            throw new ModelNotFoundException(nameof(Project), name);
        }

        public Task<Project[]> GetAllFromDepartment(IReadOnlyList<Guid> departmentProjectIds, CancellationToken cancellationToken)
        {
            if (departmentProjectIds == null)
            {
                throw new ArgumentNullException(nameof(departmentProjectIds));
            }

            cancellationToken.ThrowIfCancellationRequested();
            var set = new HashSet<Guid>(departmentProjectIds);
            var departmentProjects = projects.Find(item => set.Contains(item.Id)).ToEnumerable().ToArray();
            return Task.FromResult(departmentProjects);
        }

        public Task<Project[]> GetAllWithIds(Guid[] ids, CancellationToken cancellationToken)
        {
            if (ids == null)
            {
                throw new ArgumentNullException(nameof(ids));
            }
            
            var set = new HashSet<Guid>(ids);
            var result = projects
                .Find(item => set.Contains(item.Id))
                .ToEnumerable()
                .ToArray();
            return Task.FromResult(result);
        }
    }
}