﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Positions;
using API.ClientModels.Projects;
using Position = API.Models.Positions.Position;

namespace API.Models.Projects.Repositories
{
    public interface IProjectsRepository
    {
        Task<Project> CreateAsync(ProjectCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<Project> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<Project> GetFromDepartmentByName(IReadOnlyList<Guid> departmentPositionIds, string name,
            CancellationToken cancellationToken);
        Task<Project[]> GetAllFromDepartment(IReadOnlyList<Guid> departmentPositionIds, CancellationToken cancellationToken);
        Task<Project[]> GetAllWithIds(Guid[] ids, CancellationToken cancellationToken);
    }
}