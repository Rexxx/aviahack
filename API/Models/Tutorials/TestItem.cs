﻿namespace API.Models.Tutorials
{
    public class TestItem
    {
        public string Question { get; set; }
        public string Option0 { get; set; }
        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public int Answer { get; set; }
    }
}