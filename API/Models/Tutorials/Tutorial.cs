﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models.Tutorials
{
    public class Tutorial
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }
        
        [BsonRepresentation(BsonType.String)]
        public string URL { get; set; }

        [BsonElement("Test")]
        public IReadOnlyList<TestItem> Test;
        
        [BsonElement("Departments")]
        public IReadOnlyList<Guid> Departments;
        
        [BsonElement("Positions")]
        public IReadOnlyList<Guid> Positions;
        
        [BsonElement("Projects")]
        public Guid[] Projects;
        
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreatedAt { get; set; }
        
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime UpdatedAt { get; set; }
    }
}