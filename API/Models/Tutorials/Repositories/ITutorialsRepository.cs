﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace API.Models.Tutorials.Repositories
{
    public interface ITutorialsRepository
    {
        Task<Tutorial> CreateAsync(TutorialCreationInfo creationInfo, CancellationToken cancellationToken);
        Task<Tutorial> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<Tutorial[]> GetAllAsync(CancellationToken cancellationToken);
        Task<Tutorial> AddDepartmentAsync(Guid id, Guid departmentId, CancellationToken cancellationToken);
        Task<Tutorial> DeleteDepartmentAsync(Guid id, Guid departmentId, CancellationToken cancellationToken);
        Task<Tutorial> AddPositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken);
        Task<Tutorial> DeletePositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken);
        Task<Tutorial> AddProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken);
        Task<Tutorial> DeleteProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken);
    }
}