﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.Models.Exceptions;
using MongoDB.Driver;

namespace API.Models.Tutorials.Repositories
{
    public class MongoTutorialsRepository : ITutorialsRepository
    {
        private readonly IMongoCollection<Tutorial> tutorials;

        public MongoTutorialsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("AviahackDb");
            tutorials = database.GetCollection<Tutorial>("Tutorial");
        }

        public Task<Tutorial> CreateAsync(TutorialCreationInfo creationInfo, CancellationToken cancellationToken)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }
            
            cancellationToken.ThrowIfCancellationRequested();

            var now = DateTime.UtcNow;
            var tutorial = new Tutorial
            {
                Id = Guid.NewGuid(),
                Name = creationInfo.Name,
                URL = creationInfo.URL,
                Test = creationInfo.Test,
                Departments = new Guid[0],
                Positions = new Guid[0],
                Projects = new Guid[0],
                CreatedAt = now,
                UpdatedAt = now
            };

            tutorials.InsertOne(tutorial, cancellationToken: cancellationToken);
            return Task.FromResult(tutorial);
        }

        public Task<Tutorial> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var tutorial = tutorials.Find(item => item.Id == id).FirstOrDefault();

            if (tutorial == null)
            {
                throw new ModelNotFoundException(nameof(Tutorial), id);
            }

            return Task.FromResult(tutorial);
        }

        public Task<Tutorial[]> GetAllAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var result = tutorials.Find(item => true).ToEnumerable().ToArray();
            return Task.FromResult(result);
        }

        public async Task<Tutorial> AddDepartmentAsync(Guid id, Guid departmentId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Tutorial>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Tutorial>.Update.Push("Departments", departmentId).Set("UpdatedAt", DateTime.UtcNow);
            await tutorials.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);

            var tutorial = tutorials.Find(item => item.Id == id).FirstOrDefault();
            return tutorial;
        }

        public async Task<Tutorial> DeleteDepartmentAsync(Guid id, Guid departmentId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Tutorial>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Tutorial>.Update.Pull("Departments", departmentId).Set("UpdatedAt", DateTime.UtcNow);
            await tutorials.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
            
            var tutorial = tutorials.Find(item => item.Id == id).FirstOrDefault();
            return tutorial;
        }

        public async Task<Tutorial> AddPositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Tutorial>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Tutorial>.Update.Push("Positions", positionId).Set("UpdatedAt", DateTime.UtcNow);
            await tutorials.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
            
            var tutorial = tutorials.Find(item => item.Id == id).FirstOrDefault();
            return tutorial;
        }

        public async Task<Tutorial> DeletePositionAsync(Guid id, Guid positionId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Tutorial>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Tutorial>.Update.Pull("Positions", positionId).Set("UpdatedAt", DateTime.UtcNow);
            await tutorials.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
            
            var tutorial = tutorials.Find(item => item.Id == id).FirstOrDefault();
            return tutorial;
        }

        public async Task<Tutorial> AddProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Tutorial>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Tutorial>.Update.Push("Projects", projectId).Set("UpdatedAt", DateTime.UtcNow);
            await tutorials.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
            
            var tutorial = tutorials.Find(item => item.Id == id).FirstOrDefault();
            return tutorial;
        }

        public async Task<Tutorial> DeleteProjectAsync(Guid id, Guid projectId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var builder = Builders<Tutorial>.Filter;
            var filter = builder.Eq("Id", id);
            var update = Builders<Tutorial>.Update.Pull("Projects", projectId).Set("UpdatedAt", DateTime.UtcNow);
            await tutorials.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
            
            var tutorial = tutorials.Find(item => item.Id == id).FirstOrDefault();
            return tutorial;
        }
    }
}