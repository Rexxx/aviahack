﻿using System;

namespace API.Models.Tutorials
{
    public class TutorialCreationInfo
    {
        public string Name { get; }
        public string URL { get; }
        public TestItem[] Test { get; }

        public TutorialCreationInfo(string name, string url, TestItem[] test)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            URL = url ?? throw new ArgumentNullException(nameof(url));
            Test = test ?? new TestItem[0];
        }
    }
}