﻿using System;
using System.Linq;

namespace API.Utils
{
    public class StringUtil
    {
        private static readonly Random random = new Random();
        
        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}