﻿namespace API.Utils
{
    public static class UserUtil
    {
        public static string GetUserNameFromEmail(string email)
        {
            return email.Split('@')[0];
        }
    }
}