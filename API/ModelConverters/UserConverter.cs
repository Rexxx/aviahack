using System;
using API.Models.Users;

namespace API.ModelConverters
{
    public static class UserConverter
    {
        public static ClientModels.Users.User Convert(User modelUser)
        {
            if (modelUser == null)
            {
                throw new ArgumentNullException(nameof(modelUser));
            }

            var clientUser = new ClientModels.Users.User
            {
                Id = modelUser.Id,
                UserName = modelUser.NormalizedUserName,
                Name = modelUser.Name,
                Email = modelUser.Email,
                PhoneNumber = modelUser.PhoneNumber,
                Roles = modelUser.Roles,
                Department = modelUser.Department,
                Position = modelUser.Position,
                Projects = modelUser.Projects,
                RegisteredAt = modelUser.RegisteredAt,
                LastUpdateAt = modelUser.LastUpdateAt
            };

            return clientUser;
        }
        
        public static UserPatchInfo Convert(string username, ClientModels.Users.UserPatchInfo clientPatchInfo)
        {
            if (clientPatchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientPatchInfo));
            }

            var modelPatchInfo = new UserPatchInfo(username)
            {
                Password = clientPatchInfo.Password,
                ConfirmPassword = clientPatchInfo.ConfirmPassword
            };

            return modelPatchInfo;
        }
        
        public static UserSearchInfo Convert(ClientModels.Users.UserSearchInfo clientSearchInfo)
        {
            if (clientSearchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientSearchInfo));
            }

            var modelSearchInfo = new UserSearchInfo
            {
                Offset = clientSearchInfo.Offset,
                Limit = clientSearchInfo.Limit
            };

            return modelSearchInfo;
        }
    }
}