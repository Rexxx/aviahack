﻿using System;
using Client = API.ClientModels.Departments;
using Model = API.Models.Departments;

namespace API.ModelConverters
{
    public static class DepartmentsConverter
    {
        public static Client.Department Convert(Model.Department department)
        {
            if (department == null)
            {
                throw new ArgumentNullException(nameof(department));
            }
            
            return new Client.Department
            {
                Id = department.Id,
                Name = department.Name,
                CreatedAt = department.CreatedAt,
                UpdatedAt = department.UpdatedAt
            };
        }
    }
}