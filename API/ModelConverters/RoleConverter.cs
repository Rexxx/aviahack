using System;
using API.Models.Roles;

namespace API.ModelConverters
{
    public static class RoleConverter
    {
        public static ClientModels.Roles.Role Convert(Role modelRole)
        {
            if (modelRole == null)
            {
                throw new ArgumentNullException(nameof(modelRole));
            }

            var clientRole = new ClientModels.Roles.Role
            {
                Id = modelRole.Id,
                Name = modelRole.Name,
            };

            return clientRole;
        }
        
        public static RolePatchInfo Convert(string userName, ClientModels.Roles.RolePatchInfo clientRolePatchInfo)
        {
            if (userName == null)
            {
                throw new ArgumentNullException(nameof(userName));
            }

            if (clientRolePatchInfo == null)
            {
                throw new ArgumentNullException(nameof(clientRolePatchInfo));
            }

            var modelRolePatchInfo = new RolePatchInfo(userName, clientRolePatchInfo.UserRole);

            return modelRolePatchInfo;
        }
    }
}