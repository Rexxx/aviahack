﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models.Departments;
using API.Models.Positions;
using API.Models.Projects;
using Client = API.ClientModels.Tutorials;
using Model = API.Models.Tutorials;

namespace API.ModelConverters
{
    public static class TutorialsConverter
    {
        public static Model.TutorialCreationInfo Convert(Client.TutorialCreationInfo creationInfo)
        {
            if (creationInfo == null)
            {
                throw new ArgumentNullException(nameof(creationInfo));
            }

            Model.TestItem[] test = null;
            
            if (creationInfo.Test != null && creationInfo.Test.Length > 0)
            {
                test = creationInfo.Test.Select(item => new Model.TestItem
                {
                    Question = item.Question,
                    Option0 = item.Option0,
                    Option1 = item.Option1,
                    Option2 = item.Option2,
                    Answer = item.Answer.Value
                }).ToArray();
            }

            return new Model.TutorialCreationInfo(creationInfo.Name, creationInfo.URL, test);
        }

        public static Client.Tutorial Convert(Model.Tutorial tutorial, Department[] departments, Position[] positions, Project[] projects)
        {
            if (tutorial == null)
            {
                throw new ArgumentNullException(nameof(tutorial));
            }
            
            if (departments == null)
            {
                throw new ArgumentNullException(nameof(departments));
            }

            if (positions == null)
            {
                throw new ArgumentNullException(nameof(positions));
            }

            if (projects == null)
            {
                throw new ArgumentNullException(nameof(projects));
            }

            var clientDepartments = departments.Select(DepartmentsConverter.Convert).ToArray();

            return new Client.Tutorial
            {
                Id = tutorial.Id,
                Name = tutorial.Name,
                URL = tutorial.URL,
                Test = tutorial.Test.ToArray(),
                Departments = clientDepartments,
                Positions = positions,
                Projects = projects,
                CreatedAt = tutorial.CreatedAt,
                UpdatedAt = tutorial.UpdatedAt
            };
        }
        
        public static Client.UserTutorial Convert(Model.Tutorial tutorial)
        {
            if (tutorial == null)
            {
                throw new ArgumentNullException(nameof(tutorial));
            }
            
            return new Client.UserTutorial
            {
                Id = tutorial.Id,
                Name = tutorial.Name,
                URL = tutorial.URL,
                Test = tutorial.Test.ToArray(),
                CreatedAt = tutorial.CreatedAt,
                UpdatedAt = tutorial.UpdatedAt
            };
        }
    }
}