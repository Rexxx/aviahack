using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Roles;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Role = API.Models.Roles.Role;
using RoleConverter = API.ModelConverters.RoleConverter;
using User = API.Models.Users.User;

namespace API.Controllers
{
    [Route("api/roles")]
    public class RolesController : ControllerBase
    {
        private readonly RoleManager<Role> roleManager;
        private readonly UserManager<User> userManager;

        public RolesController(RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            this.roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        /// <summary>
        /// Gets roles list
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Authorize(Roles = "admin")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetAllRoles(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var roles = await Task.Run(() => roleManager.Roles, cancellationToken);

            if (roles == null)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Role), "Roles not found.");
                return NotFound(error);
            }

            var clientRoles = roles.Select(item => RoleConverter.Convert(item)).ToImmutableList();
            return Ok(clientRoles);
        }

        /// <summary>
        /// Patches user role
        /// Removes role if user has is, or adds role to user otherwise
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="clientRolePatchInfo">Role patch info</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{userName}")]
        [Authorize(Roles = "admin")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PatchRoleAsync([FromRoute]string userName, 
            [FromBody] RolePatchInfo clientRolePatchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(RolePatchInfo), message);
                return BadRequest(error);
            }

            var user = await userManager.FindByNameAsync(userName);
            
            if (user == null)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Models.Users.User.UserName), $"User with name '{userName}' not found.");
                return NotFound(error);
            }

            var modelRolePatchInfo = RoleConverter.Convert(userName, clientRolePatchInfo);
            var modelRole = await roleManager.FindByNameAsync(modelRolePatchInfo.UserRole.ToLower());

            if (modelRole == null)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Models.Users.User), $"Role with name '{modelRolePatchInfo.UserRole}' not found.");
                return NotFound(error);
            }

            if (user.Roles.Contains(modelRolePatchInfo.UserRole.ToUpper()))
            {
                await userManager.RemoveFromRoleAsync(user, modelRolePatchInfo.UserRole);
            }
            else
            {
                await userManager.AddToRoleAsync(user, modelRolePatchInfo.UserRole);
            }

            return Ok(user);
        }
    }
}