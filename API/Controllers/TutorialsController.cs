﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Departments;
using API.ClientModels.Positions;
using API.ClientModels.Projects;
using API.ClientModels.Tutorials;
using API.ModelConverters;
using API.Models.Departments.Repositories;
using API.Models.Exceptions;
using API.Models.Positions;
using API.Models.Positions.Repositories;
using API.Models.Projects;
using API.Models.Projects.Repositories;
using API.Models.Tutorials.Repositories;
using API.Models.Users;
using API.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Department = API.Models.Departments.Department;

namespace API.Controllers
{
    [Route("api/tutorials")]
    public class TutorialsController : ControllerBase
    {
        private readonly ITutorialsRepository repository;
        private readonly IDepartmentsRepository departmentsRepository;
        private readonly IPositionsRepository positionsRepository;
        private readonly IProjectsRepository projectsRepository;
        private readonly UserManager<User> userManager;

        public TutorialsController(ITutorialsRepository repository, IDepartmentsRepository departmentsRepository, 
            IPositionsRepository positionsRepository, IProjectsRepository projectsRepository, UserManager<User> userManager)
        {
            this.repository = repository;
            this.departmentsRepository = departmentsRepository;
            this.positionsRepository = positionsRepository;
            this.projectsRepository = projectsRepository;
            this.userManager = userManager ?? throw new ArgumentException(nameof(userManager));
        }
        
        /// <summary>
        /// Creates tutorial
        /// </summary>
        /// <param name="creationInfo">Tutorial creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateTutorialAsync([FromBody]TutorialCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(TutorialCreationInfo), message);
                return BadRequest(error);
            }

            Models.Tutorials.Tutorial tutorial;

            var modelCreationInfo = TutorialsConverter.Convert(creationInfo);

            try
            {
                tutorial =
                    await repository.CreateAsync(modelCreationInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelDuplicationException ex)
            {
                var error = ErrorResponsesService.DuplicationError(nameof(Tutorial), ex.Message);
                return BadRequest(error);
            }

            // всё равно при создании эти списки будут пустыми
            var clientTutorial = TutorialsConverter.Convert(tutorial, new Department[0], new Position[0], new Project[0]);
            return CreatedAtRoute("GetTutorialRoute", new { id = tutorial.Id }, clientTutorial);
        }
        
        /// <summary>
        /// Returns a tutorial by id
        /// </summary>
        /// <param name="id">Tutorial id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetTutorialRoute")]
        public async Task<IActionResult> GetTutorialAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Models.Tutorials.Tutorial tutorial;

            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Department), ex.Message);
                return NotFound(error);
            }

            var departments = await departmentsRepository.GetAllWithIdsAsync(tutorial.Departments, cancellationToken)
                .ConfigureAwait(false);
            var positions = await positionsRepository.GetAllWithIds(tutorial.Positions, cancellationToken)
                .ConfigureAwait(false);
            var projects = await projectsRepository.GetAllWithIds(tutorial.Projects, cancellationToken)
                .ConfigureAwait(false);
            var clientTutorial = TutorialsConverter.Convert(tutorial, departments, positions, projects);
            return Ok(clientTutorial);
        }
        
        /// <summary>
        /// Returns a tutorials for user
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetTutorialsForUserAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var userName = HttpContext.User.Identity.Name;
            var user = await userManager.FindByNameAsync(userName);
            
            if (user == null)
            {
                var error = ErrorResponsesService.ValidationError("User", "Unauthorized access prohibited.");
                return Unauthorized(error);
            }

            var tutorials = await repository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var userDepartment = await departmentsRepository.GetByNameAsync(user.Department, cancellationToken)
                .ConfigureAwait(false);
            var userPosition = await positionsRepository
                .GetFromDepartmentByName(userDepartment.Positions, user.Position, cancellationToken)
                .ConfigureAwait(false);
            var userProjects = await projectsRepository.GetAllFromDepartment(userDepartment.Projects, cancellationToken)
                .ConfigureAwait(false);
            var userProjectIds = userProjects.Where(item => user.Projects.Contains(item.Name)).Select(item=>item.Id).ToArray();

            var modelTutorials = tutorials.Where(item =>
                item.Departments.Contains(userDepartment.Id) || item.Positions.Contains(userPosition.Id) ||
                item.Projects.Any(guid => userProjectIds.Contains(guid)));

            var clientTutorials = modelTutorials.Select(TutorialsConverter.Convert).ToArray();
            var completedTutorials = user.CompletedTutorials;

            foreach (var clientTutorial in clientTutorials)
            {
                var completed = completedTutorials.FirstOrDefault(i => i == clientTutorial.Id);

                if (completed != Guid.Empty)
                {
                    clientTutorial.Completed = true;
                }
            }
            return Ok(clientTutorials);
        }

        /// <summary>
        /// Makes tutorial completed by id
        /// </summary>
        /// <param name="id">Tutorial id</param>
        /// <param name="cancellationToken"></param>
        [HttpPatch]
        [Route("{id}")]
        public async Task<IActionResult> CompleteTutorialAsync([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            
            cancellationToken.ThrowIfCancellationRequested();

            var userName = HttpContext.User.Identity.Name;
            var user = await userManager.FindByNameAsync(userName);
            
            if (user == null)
            {
                var error = ErrorResponsesService.ValidationError("User", "Unauthorized access prohibited.");
                return Unauthorized(error);
            }

            Models.Tutorials.Tutorial tutorial;
            
            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Department), ex.Message);
                return NotFound(error);
            }
            
            user.CompletedTutorials = new List<Guid>(user.CompletedTutorials) {id};
            user.LastUpdateAt = DateTime.UtcNow;
            await userManager.UpdateAsync(user);
            var clientTutorial = TutorialsConverter.Convert(tutorial);
            clientTutorial.Completed = true;
            return Ok(clientTutorial);
        }

        [HttpPost]
        [Route("{id}/add-department")]
        public async Task<ActionResult> AddDepartmentToTutorialAsync([FromRoute] Guid id, [FromBody] DepartmentInfo departmentInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DepartmentInfo), message);
                return BadRequest(error);
            }

            Models.Tutorials.Tutorial tutorial;

            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Tutorial), ex.Message);
                return NotFound(error);
            }
            
            try
            {
                var department = await departmentsRepository.GetByNameAsync(departmentInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
                tutorial = await repository
                    .AddDepartmentAsync(id, department.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ProjectInfo), ex.Message);
                return NotFound(error);
            }

            var departments = await departmentsRepository.GetAllWithIdsAsync(tutorial.Departments, cancellationToken)
                .ConfigureAwait(false);
            var positions = await positionsRepository.GetAllWithIds(tutorial.Positions, cancellationToken)
                .ConfigureAwait(false);
            var projects = await projectsRepository.GetAllWithIds(tutorial.Projects, cancellationToken)
                .ConfigureAwait(false);
            var clientTutorial = TutorialsConverter.Convert(tutorial, departments, positions, projects);
            return Ok(clientTutorial);
        }
        
        [HttpPost]
        [Route("{id}/delete-department")]
        public async Task<ActionResult> DeleteDepartmentFromTutorialAsync([FromRoute] Guid id, [FromBody] DepartmentInfo departmentInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DepartmentInfo), message);
                return BadRequest(error);
            }

            Models.Tutorials.Tutorial tutorial;

            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Tutorial), ex.Message);
                return NotFound(error);
            }
            
            try
            {
                var department = await departmentsRepository.GetByNameAsync(departmentInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
                tutorial = await repository
                    .DeleteDepartmentAsync(id, department.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ProjectInfo), ex.Message);
                return NotFound(error);
            }

            var departments = await departmentsRepository.GetAllWithIdsAsync(tutorial.Departments, cancellationToken)
                .ConfigureAwait(false);
            var positions = await positionsRepository.GetAllWithIds(tutorial.Positions, cancellationToken)
                .ConfigureAwait(false);
            var projects = await projectsRepository.GetAllWithIds(tutorial.Projects, cancellationToken)
                .ConfigureAwait(false);
            var clientTutorial = TutorialsConverter.Convert(tutorial, departments, positions, projects);
            return Ok(clientTutorial);
        }
        
        [HttpPost]
        [Route("{id}/add-position")]
        public async Task<ActionResult> AddPositionToTutorialAsync([FromRoute] Guid id, [FromBody] PositionInfo positionInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(PositionInfo), message);
                return BadRequest(error);
            }

            Models.Tutorials.Tutorial tutorial;

            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Tutorial), ex.Message);
                return NotFound(error);
            }
            
            try
            {
                var department = await departmentsRepository.GetByNameAsync(positionInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
                var departmentPosition = await positionsRepository.GetFromDepartmentByName(department.Positions, positionInfo.Position, cancellationToken)
                    .ConfigureAwait(false);
                
                tutorial = await repository
                    .AddPositionAsync(id, departmentPosition.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ProjectInfo), ex.Message);
                return NotFound(error);
            }

            var departments = await departmentsRepository.GetAllWithIdsAsync(tutorial.Departments, cancellationToken)
                .ConfigureAwait(false);
            var positions = await positionsRepository.GetAllWithIds(tutorial.Positions, cancellationToken)
                .ConfigureAwait(false);
            var projects = await projectsRepository.GetAllWithIds(tutorial.Projects, cancellationToken)
                .ConfigureAwait(false);
            var clientTutorial = TutorialsConverter.Convert(tutorial, departments, positions, projects);
            return Ok(clientTutorial);
        }
        
        [HttpPost]
        [Route("{id}/delete-position")]
        public async Task<ActionResult> DeletePositionFromTutorialAsync([FromRoute] Guid id, [FromBody] PositionInfo positionInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(PositionInfo), message);
                return BadRequest(error);
            }

            Models.Tutorials.Tutorial tutorial;

            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Tutorial), ex.Message);
                return NotFound(error);
            }
            
            try
            {
                var department = await departmentsRepository.GetByNameAsync(positionInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
                var departmentPosition = await positionsRepository.GetFromDepartmentByName(department.Positions, positionInfo.Position, cancellationToken)
                    .ConfigureAwait(false);

                tutorial = await repository
                    .DeletePositionAsync(id, departmentPosition.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ProjectInfo), ex.Message);
                return NotFound(error);
            }

            var departments = await departmentsRepository.GetAllWithIdsAsync(tutorial.Departments, cancellationToken)
                .ConfigureAwait(false);
            var positions = await positionsRepository.GetAllWithIds(tutorial.Positions, cancellationToken)
                .ConfigureAwait(false);
            var projects = await projectsRepository.GetAllWithIds(tutorial.Projects, cancellationToken)
                .ConfigureAwait(false);
            var clientTutorial = TutorialsConverter.Convert(tutorial, departments, positions, projects);
            return Ok(clientTutorial);
        }
        
        [HttpPost]
        [Route("{id}/add-project")]
        public async Task<ActionResult> AddProjectToTutorialAsync([FromRoute] Guid id, [FromBody] ProjectInfo projectInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(ProjectInfo), message);
                return BadRequest(error);
            }

            Models.Tutorials.Tutorial tutorial;

            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Tutorial), ex.Message);
                return NotFound(error);
            }
            
            try
            {
                var department = await departmentsRepository.GetByNameAsync(projectInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
                var departmentProject = await projectsRepository.GetFromDepartmentByName(department.Projects, projectInfo.Project, cancellationToken)
                    .ConfigureAwait(false);
                
                tutorial = await repository
                    .AddProjectAsync(id, departmentProject.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ProjectInfo), ex.Message);
                return NotFound(error);
            }

            var departments = await departmentsRepository.GetAllWithIdsAsync(tutorial.Departments, cancellationToken)
                .ConfigureAwait(false);
            var positions = await positionsRepository.GetAllWithIds(tutorial.Positions, cancellationToken)
                .ConfigureAwait(false);
            var projects = await projectsRepository.GetAllWithIds(tutorial.Projects, cancellationToken)
                .ConfigureAwait(false);
            var clientTutorial = TutorialsConverter.Convert(tutorial, departments, positions, projects);
            return Ok(clientTutorial);
        }
        
        [HttpPost]
        [Route("{id}/delete-project")]
        public async Task<ActionResult> DeleteProjectFromTutorialAsync([FromRoute] Guid id, [FromBody] ProjectInfo projectInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(PositionInfo), message);
                return BadRequest(error);
            }

            Models.Tutorials.Tutorial tutorial;

            try
            {
                tutorial = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Tutorial), ex.Message);
                return NotFound(error);
            }
            
            try
            {
                var department = await departmentsRepository.GetByNameAsync(projectInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
                var departmentProject = await projectsRepository.GetFromDepartmentByName(department.Projects, projectInfo.Project, cancellationToken)
                    .ConfigureAwait(false);

                tutorial = await repository
                    .DeleteProjectAsync(id, departmentProject.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(ProjectInfo), ex.Message);
                return NotFound(error);
            }

            var departments = await departmentsRepository.GetAllWithIdsAsync(tutorial.Departments, cancellationToken)
                .ConfigureAwait(false);
            var positions = await positionsRepository.GetAllWithIds(tutorial.Positions, cancellationToken)
                .ConfigureAwait(false);
            var projects = await projectsRepository.GetAllWithIds(tutorial.Projects, cancellationToken)
                .ConfigureAwait(false);
            var clientTutorial = TutorialsConverter.Convert(tutorial, departments, positions, projects);
            return Ok(clientTutorial);
        }
        
        
    }
}