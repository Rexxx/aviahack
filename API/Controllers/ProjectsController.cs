﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.Departments;
using API.ClientModels.Projects;
using API.Models.Departments.Repositories;
using API.Models.Exceptions;
using API.Models.Projects;
using API.Models.Projects.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/projects")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsRepository repository;
        private readonly IDepartmentsRepository departmentsRepository;

        public ProjectsController(IProjectsRepository repository, IDepartmentsRepository departmentsRepository)
        {
            this.repository = repository;
            this.departmentsRepository = departmentsRepository;
        }
        
        /// <summary>
        /// Creates project
        /// </summary>
        /// <param name="creationInfo">Project creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateProjectAsync([FromBody]ProjectCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(ProjectCreationInfo), message);
                return BadRequest(error);
            }

            Models.Departments.Department department;
            
            try
            {
                department = await departmentsRepository.GetByNameAsync(creationInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Department), ex.Message);
                return NotFound(error);
            }

            Project project;

            try
            {
                project =
                    await repository.CreateAsync(creationInfo, cancellationToken).ConfigureAwait(false);
                await departmentsRepository.AddProjectAsync(department.Id, project.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelDuplicationException ex)
            {
                var error = ErrorResponsesService.DuplicationError(nameof(Project), ex.Message);
                return BadRequest(error);
            }

            return CreatedAtRoute("GetProjectRoute", new { id = project.Id }, project);
        }
        
        /// <summary>
        /// Returns a project by id
        /// </summary>
        /// <param name="id">Project id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetProjectRoute")]
        public async Task<IActionResult> GetProjectAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Project project;

            try
            {
                project = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Project), ex.Message);
                return NotFound(error);
            }

            return Ok(project);
        }
        
        /// <summary>
        /// Returns a list of projects from department
        /// </summary>
        /// <param name="searchInfo">Department name</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("all")]
        public async Task<IActionResult> GetProjectsFromDepartmentAsync([FromBody]DepartmentSearchInfo searchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DepartmentSearchInfo), message);
                return BadRequest(error);
            }

            Models.Departments.Department departmentModel;
            
            try
            {
                departmentModel = await departmentsRepository.GetByNameAsync(searchInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(searchInfo.Department), ex.Message);
                return NotFound(error);
            }
            
            var projects = await repository
                .GetAllFromDepartment(departmentModel.Projects, cancellationToken)
                .ConfigureAwait(false);

            var clientProject = projects.Select(item => item.Name).ToArray();
            return Ok(clientProject);
        }
    }
}