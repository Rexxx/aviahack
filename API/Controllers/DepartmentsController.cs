﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels;
using API.ClientModels.Departments;
using API.ModelConverters;
using API.Models.Departments.Repositories;
using API.Models.Exceptions;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using Department = API.Models.Departments.Department;

namespace API.Controllers
{
    [Route("api/departments")]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentsRepository repository;

        public DepartmentsController(IDepartmentsRepository repository)
        {
            this.repository = repository;
        }
        
        /// <summary>
        /// Creates department
        /// </summary>
        /// <param name="creationInfo">Department creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateDepartmentAsync([FromBody]DepartmentCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DepartmentCreationInfo), message);
                return BadRequest(error);
            }

            Department department;

            try
            {
                department =
                    await repository.CreateAsync(creationInfo, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelDuplicationException ex)
            {
                var error = ErrorResponsesService.DuplicationError(nameof(Department), ex.Message);
                return BadRequest(error);
            }

            var clientDepartment = DepartmentsConverter.Convert(department);
            return CreatedAtRoute("GetDepartmentRoute", new { id = department.Id }, clientDepartment);
        }
        
        /// <summary>
        /// Returns a department by id
        /// </summary>
        /// <param name="id">Department id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetDepartmentRoute")]
        public async Task<IActionResult> GetDepartmentAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Department department;

            try
            {
                department = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Department), ex.Message);
                return NotFound(error);
            }

            var clientDepartment = DepartmentsConverter.Convert(department);
            return Ok(clientDepartment);
        }
        
        /// <summary>
        /// Returns all departments
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            var departments = await repository.GetAllAsync(cancellationToken).ConfigureAwait(false);
            var list = departments.Select(item => item.Name).ToArray();

            return Ok(list);
        }
    }
}