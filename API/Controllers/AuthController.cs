using System;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels.UserIdentity;
using API.Models.Users;
using API.Services;
using API.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<User> signInManager;
        
        public AuthController(SignInManager<User> signInManager)
        {
            this.signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
        }
        
        /// <summary>
        /// Authenticates user
        /// </summary>
        /// <param name="loginInfoInfo">User info for sign in</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Login([FromBody] LoginInfo loginInfoInfo, 
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(LoginInfo), message);
                return BadRequest(error);
            }

            var userName = UserUtil.GetUserNameFromEmail(loginInfoInfo.Email);
            var result = await signInManager.PasswordSignInAsync(userName, loginInfoInfo.Password, 
                true, false);
            
            if (!result.Succeeded)
            {
                var error = ErrorResponsesService.InvalidCredentialsError(nameof(loginInfoInfo));
                return BadRequest(error);
            }

            return Ok(result);
        }

        /// <summary>
        /// Ends the current users session
        /// </summary>
        [HttpPost]
        [Authorize]
        [Route("logout")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return Ok();
        }
    }
}