﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using API.ClientModels;
using API.ClientModels.Departments;
using API.ClientModels.Positions;
using API.ModelConverters;
using API.Models.Departments.Repositories;
using API.Models.Positions.Repositories;
using API.Models.Exceptions;
using API.Models.Positions;
using API.Models.Positions.Repositories;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using Department = API.Models.Departments.Department;
using Position = API.Models.Positions.Position;

namespace API.Controllers
{
    [Route("api/positions")]
    public class PositionsController : ControllerBase
    {
        private readonly IPositionsRepository repository;
        private readonly IDepartmentsRepository departmentsRepository;

        public PositionsController(IPositionsRepository repository, IDepartmentsRepository departmentsRepository)
        {
            this.repository = repository;
            this.departmentsRepository = departmentsRepository;
        }
        
        /// <summary>
        /// Creates position
        /// </summary>
        /// <param name="creationInfo">Position creation info</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreatePositionAsync([FromBody]PositionCreationInfo creationInfo,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(PositionCreationInfo), message);
                return BadRequest(error);
            }

            Department department;
            
            try
            {
                department = await departmentsRepository.GetByNameAsync(creationInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Department), ex.Message);
                return NotFound(error);
            }

            Position position;

            try
            {
                position =
                    await repository.CreateAsync(creationInfo, cancellationToken).ConfigureAwait(false);
                await departmentsRepository.AddPositionAsync(department.Id, position.Id, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelDuplicationException ex)
            {
                var error = ErrorResponsesService.DuplicationError(nameof(Position), ex.Message);
                return BadRequest(error);
            }

            return CreatedAtRoute("GetPositionRoute", new { id = position.Id }, position);
        }
        
        /// <summary>
        /// Returns a position by id
        /// </summary>
        /// <param name="id">Position id</param>
        /// <param name="cancellationToken"></param>
        [HttpGet]
        [Route("{id}", Name = "GetPositionRoute")]
        public async Task<IActionResult> GetPositionAsync([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Position position;

            try
            {
                position = await repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(Position), ex.Message);
                return NotFound(error);
            }

            return Ok(position);
        }
        
        /// <summary>
        /// Returns a list of positions from department
        /// </summary>
        /// <param name="searchInfo">Department name</param>
        /// <param name="cancellationToken"></param>
        [HttpPost]
        [Route("all")]
        public async Task<IActionResult> GetPositionsFromDepartmentAsync([FromBody]DepartmentSearchInfo searchInfo, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (!ModelState.IsValid)
            {
                var message = ErrorResponsesService.GetErrorMessage(ModelState);
                var error = ErrorResponsesService.BadRequest(nameof(DepartmentSearchInfo), message);
                return BadRequest(error);
            }

            Department departmentModel;
            
            try
            {
                departmentModel = await departmentsRepository.GetByNameAsync(searchInfo.Department, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (ModelNotFoundException ex)
            {
                var error = ErrorResponsesService.NotFoundError(nameof(searchInfo.Department), ex.Message);
                return NotFound(error);
            }
            
            var positions = await repository
                .GetAllFromDepartment(departmentModel.Positions, cancellationToken)
                .ConfigureAwait(false);

            var clientPosition = positions.Select(item => item.Name).ToArray();
            return Ok(clientPosition);
        }
    }
}