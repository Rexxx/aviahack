using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.UserIdentity
{
    public class LoginInfo
    {
        [Required(ErrorMessage = "Field 'email' is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Field 'password' is required")]
        public string Password { get; set; }
    }
}