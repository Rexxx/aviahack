using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Users
{
    public class UserCreationInfo
    {
        [Required(ErrorMessage = "Field 'email' is required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Field 'name' is required")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Field 'department' is required")]
        public string Department { get; set; }
        
        [Required(ErrorMessage = "Field 'position' is required")]
        public string Position { get; set; }

        [Required(ErrorMessage = "Field 'password' is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Field 'confirmPassword' is required")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}