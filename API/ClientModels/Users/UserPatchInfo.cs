using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Users
{
    public class UserPatchInfo
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        
        public string Department { get; set; }
        
        public string Position { get; set; }
    }
}