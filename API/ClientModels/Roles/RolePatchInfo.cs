using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Roles
{
    public class RolePatchInfo
    {
        [Required(ErrorMessage = "Field 'userRole' is required")]
        public string UserRole { get; set; }
    }
}