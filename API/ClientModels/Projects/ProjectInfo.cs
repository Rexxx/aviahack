﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Projects
{
    public class ProjectInfo
    {
        [Required(ErrorMessage = "Field 'project' is required")]
        public string Project { get; set; }
        
        [Required(ErrorMessage = "Field 'department' is required")]
        public string Department { get; set; }
    }
}