﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Positions
{
    public class PositionInfo
    {
        [Required(ErrorMessage = "Field 'position' is required")]
        public string Position { get; set; }
        
        [Required(ErrorMessage = "Field 'department' is required")]
        public string Department { get; set; }
    }
}