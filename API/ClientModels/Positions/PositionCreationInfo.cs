﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Positions
{
    public class PositionCreationInfo
    {
        [Required(ErrorMessage = "Field 'name' is required")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Field 'department' is required")]
        public string Department { get; set; }
    }
}