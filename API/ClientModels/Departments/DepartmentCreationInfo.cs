﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Departments
{
    public class DepartmentCreationInfo
    {
        [Required(ErrorMessage = "Field 'name' is required")]
        public string Name { get; set; }
    }
}