﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Departments
{
    public class DepartmentSearchInfo
    {
        [Required(ErrorMessage = "Field 'name' is required")]
        public string Department { get; set; }
    }
}