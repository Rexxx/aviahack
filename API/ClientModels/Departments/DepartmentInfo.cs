﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Departments
{
    public class DepartmentInfo
    {
        [Required(ErrorMessage = "Field 'department' is required")]
        public string Department { get; set; }
    }
}