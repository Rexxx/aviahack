﻿using System;
using API.ClientModels.Departments;
using API.Models.Positions;
using API.Models.Projects;

namespace API.ClientModels.Tutorials
{
    public class Tutorial
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public Models.Tutorials.TestItem[] Test { get; set; }
        public Department[] Departments { get; set; }
        public Position[] Positions { get; set; }
        public Project[] Projects { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}