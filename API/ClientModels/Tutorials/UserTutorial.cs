﻿using System;

namespace API.ClientModels.Tutorials
{
    public class UserTutorial
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public Models.Tutorials.TestItem[] Test { get; set; }
        public bool Completed { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}