﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Tutorials
{
    public class TestItem
    {
        [Required(ErrorMessage = "Field 'question' is required")]
        public string Question { get; set; }
        
        [Required(ErrorMessage = "Field 'option0' is required")]
        public string Option0 { get; set; }
        
        [Required(ErrorMessage = "Field 'option1' is required")]
        public string Option1 { get; set; }
        
        [Required(ErrorMessage = "Field 'option2' is required")]
        public string Option2 { get; set; }
        
        [Required(ErrorMessage = "Field 'answer' is required")]
        [Range(0, 2, ErrorMessage = "Answer value must be in range 0 and 2")]
        public int? Answer { get; set; }
    }
}