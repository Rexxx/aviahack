﻿using System.ComponentModel.DataAnnotations;

namespace API.ClientModels.Tutorials
{
    public class TutorialCreationInfo
    {
        [Required(ErrorMessage = "Field 'name' is required")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Field 'url' is required")]
        public string URL { get; set; }
        
        public TestItem[] Test { get; set; }
    }
}