Регистрация: `POST` `https://lapkisoft.me/api/users` `{"email": "anal@lapkisoft.me", "name": "Василий Пупкин Артурович", "department": "Финансы", "position": "Аналитик", "password": "qwe123", "confirmPassword": "qwe123"}`

Авторизация: `POST` `https://lapkisoft.me/api/auth/login` `{"email": "anal@lapkisoft.me", "password": "qwe123"}`

Выход: `POST` `https://lapkisoft.me/api/auth/logout` `{}`

Получить информацию о текущем пользователе: `GET` `https://lapkisoft.me/api/users/info` `{}`

Создать отдел: `POST` `https://lapkisoft.me/api/departments` `{"name": "Финансы"}`

Получить список всех отделов: `GET` `https://lapkisoft.me/api/departments` `{}`

Создать должность: `POST` `https://lapkisoft.me/api/positions` `{"name": "Аналитик", "department": "Финансы"}`

Получить список всех должностей в отделе: `POST` `https://lapkisoft.me/api/positions/all` `{"department": "Финансы"}`

Создать проект: `POST` `https://lapkisoft.me/api/projects` `{"name": "Касса", "department": "Финансы"}`

Получить список всех проектов в отделе: `POST` `https://lapkisoft.me/api/projects/all` `{"department": "Финансы"}`

Добавить проект пользователю: `POST` `https://lapkisoft.me/api/users/{userName}/add-project` `{"department": "Финансы", "project": "Касса"}`

Удалить проект у пользователя: `POST` `https://lapkisoft.me/api/users/{userName}/delete-project` `{"department": "Финансы", "project": "Касса"}`

Создать задачу: `POST` `https://lapkisoft.me/api/tutorials` `{"name": "Познакомиться с основами предметной области", "url": "https://lapkisoft.me/swagger/index.html", "test": [{"question": "Корреспондирующие счета не включают в себя...", "answer": 0, "option0": "Универсальные счета", "option1": "Активные счета", "option2": "Пассивные счета"}]}`

з.ы. поле test может быть null или пустым массивом

Добавить отдел к задаче: `POST` `https://lapkisoft.me/api/tutorials/{id}/add-department` `{"department": "Финансы"}`

Удалить отдел из задачи: `POST` `https://lapkisoft.me/api/tutorials/{id}/delete-department` `{"department": "Финансы"}`

Добавить должность к задаче: `POST` `https://lapkisoft.me/api/tutorials/{id}/add-position` `{"department": "Финансы", "position": "Аналитик"}`

Удалить должность из задачи: `POST` `https://lapkisoft.me/api/tutorials/{id}/delete-position` `{"department": "Финансы", "position": "Аналитик"}`

Добавить проект к задаче: `POST` `https://lapkisoft.me/api/tutorials/{id}/add-project` `{"department": "Финансы", "project": "Касса"}`

Удалить проект из задачи: `POST` `https://lapkisoft.me/api/tutorials/{id}/delete-project` `{"department": "Финансы", "project": "Касса"}`

Завершить задачу пользователем: `PATCH` `https://lapkisoft.me/api/tutorials/{id}` `{}`